from django.db import models
from djrichtextfield.models import RichTextField
from model_utils import FieldTracker
# Create your models here.
STATUS_CHOICES = [
  ('draft', 'draft'),
  ('booth', 'booth'),
  ('employee', 'employee'),
  ('leader', 'leader'),
  ('submitted', 'submitted')
]


class Article(models.Model):
  name = models.CharField(max_length=100)
  content = RichTextField()
  accepted = models.BooleanField(blank=True, default=False)
  tracker = FieldTracker(fields=['accepted'])
  status = models.CharField(max_length=10, choices=STATUS_CHOICES, default="draft")

  def __str__(self):
    return self.name


class ArticleStatusLog(models.Model):
  article = models.ForeignKey(Article, on_delete= models.CASCADE, related_name="Article")
  status = models.CharField(max_length=10, choices=STATUS_CHOICES, default="draft")
  time_stamp_field = models.DateTimeField()

  def __str__(self):
    return f"{self.article.name} - {self.status} - {self.time_stamp_field}"


class BoothManager(models.Manager):
  def get_queryset(self):
    return super(BoothManager, self).get_queryset().filter(
      status='draft')


class EmployeeManager(models.Manager):
  def get_queryset(self):
    return super(EmployeeManager, self).get_queryset().filter(
      status='booth')


class LeaderManager(models.Manager):
  def get_queryset(self):
    return super(LeaderManager, self).get_queryset().filter(
      status='employee')


class SubmittedManager(models.Manager):
  def get_queryset(self):
    return super(SubmittedManager, self).get_queryset().filter(
      status='leader')


class Booth(Article):
  objects = BoothManager()

  class Meta:
    proxy = True


class Employee(Article):
  objects = EmployeeManager()

  class Meta:
    proxy = True


class Leader(Article):
  objects = LeaderManager()

  class Meta:
    proxy = True


class Submitted(Article):
  objects = SubmittedManager()

  class Meta:
    proxy = True



from django.contrib import admin
from .models import Article, Booth, Employee, Leader, ArticleStatusLog
from django.contrib.auth import get_permission_codename
from django.contrib import messages
from django.utils.translation import ngettext
# Register your models here.


class BoothAdmin(admin.ModelAdmin):

  fieldsets = [
    ('Title', {'fields': ['name']}),
    ('Body', {'fields': ['content']}),
    ('Status', {'fields': ['status']}),
  ]

  list_display = ('name', 'content')
  actions = ['accept']

  def accept(self, request, queryset):
    queryset.update(status="booth")
    # self.message_user(request, ngettext(
    #   '%d article was successfully marked as approved.',
    #   update,
    # ) % update, messages.SUCCESS)

  accept.allowed_permissions = ('accept',)

  def has_accept_permission(self, request):
    opts = self.opts
    codename = get_permission_codename('accept', opts)
    return request.user.has_perm('%s.%s' % (opts.app_label, codename))


class EmployeeAdmin(admin.ModelAdmin):
  fieldsets = [
    ('Title', {'fields': ['name']}),
    ('Body', {'fields': ['content']}),
    ('Status', {'fields': ['status']}),
  ]

  list_display = ('name', 'content')
  # if Article.accepted_Booth == True:
  actions = ['accept']

  def accept(self, request, queryset):
    queryset.update(status="leader")

  accept.allowed_permissions = ('accept',)

  def has_accept_permission(self, request):
    opts = self.opts
    codename = get_permission_codename('accept', opts)
    return request.user.has_perm('%s.%s' % (opts.app_label, codename))


class LeaderAdmin(admin.ModelAdmin):
  fieldsets = [
    ('Title', {'fields': ['name']}),
    ('Body', {'fields': ['content']}),
    # ('Status', {'fields': ['status']}),
    # ('Accepted from Booth', {'fields': ['accepted_Booth']}),
    # ('Accepted from Employee', {'fields': ['accepted_Employee']}),
    # ('Accepted from Employee', {'fields': ['accepted_Leader']})
  ]

  list_display = ('name', 'content')
  # if Article.accepted_Employee == True:
  actions = ['accept']

  def accept(self, request, queryset):
    queryset.update(status="submitted")
    update = queryset.update(accepted=True)

  accept.allowed_permissions = ('accept',)

  def has_accept_permission(self, request):
    opts = self.opts
    codename = get_permission_codename('accept', opts)
    return request.user.has_perm('%s.%s' % (opts.app_label, codename))


class ArticleStatusLogInLine(admin.TabularInline):
  model = ArticleStatusLog
  extra = 0


class ArticleAdmin(admin.ModelAdmin):
  fieldsets = [
    ('Title', {'fields': ['name']}),
    ('Body', {'fields': ['content']}),
  ]
  in_lines = [
    ArticleStatusLogInLine,
  ]
  list_display = ('name', 'get_status', )

  def get_status(self, obj):
    status = ArticleStatusLog.objects.get(id=obj.id)
    return str(status)


  # def formfield_for_choice_field(self, db_field, request, **kwargs):
  #   if db_field.name == "status":
  #     kwargs['choices'] = (
  #       ('draft', 'draft'),
  #     )

  # if Article.accepted_Leader == True:
  #   Article.result = "Accepted"
    # Article.status = "Published"



admin.site.register(Booth, BoothAdmin)
admin.site.register(Employee, EmployeeAdmin)
admin.site.register(Leader, LeaderAdmin)
admin.site.register(Article, ArticleAdmin)
admin.site.register(ArticleStatusLog)
